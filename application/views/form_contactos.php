<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Crear Contacto</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?= link_tag('assets/css/site.css'); ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="create-contacto m-5">
            <div class="row">
                <div class="col-12">
                    <h2><?= (!isset($contacto)?"Crear":"Modificar")?> Contacto</h2>
                </div>
            </div>

            <br><br>

            <div class="create-form">
                <?php //echo validation_errors(); ?>
                <?= form_open() ?>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?= form_label('Nombre', 'nombre', array('class' => 'control-label') ) ?>
                            <?= form_input('nombre', set_value('nombre', @$contacto->nombre), array('class' => 'form-control', 'id' => 'nombre')) ?>
                            <?= form_error('nombre', '<div class="field-validation-error">', '</div>') ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= form_label('Email', 'email', array('class' => 'control-label') ) ?>
                            <?= form_input('email', set_value('email', @$contacto->email), array('class' => 'form-control', 'id' => 'email')) ?>
                            <?= form_error('email', '<div class="field-validation-error">', '</div>') ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= form_label('Telefono', 'telefono', array('class' => 'control-label') ) ?>
                            <?= form_input('telefono', set_value('telefono', @$contacto->telefono), array('class' => 'form-control', 'id' => 'telefono')) ?>
                            <?= form_error('telefono', '<div class="field-validation-error">', '</div>') ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= form_label('Edad', 'edad', array('class' => 'control-label') ) ?>
                            <?= form_input('edad', set_value('edad', @$contacto->edad), array('class' => 'form-control', 'id' => 'edad')) ?>
                            <?= form_error('edad', '<div class="field-validation-error">', '</div>') ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?= form_label('Estatus', 'estatus', array('class' => 'control-label') ) ?>
                            <?= form_dropdown('estatus', array('' => '', 0 => 'Inactivo', 1 => 'Activo'), set_value('estatus', @$contacto->estatus), array('class' => 'form-control', 'id' => 'estatus', 'placeholder' => 'Seleccione')) ?>
                            <?= form_error('estatus', '<div class="field-validation-error">', '</div>') ?>
                        </div>
                    </div>
                    <br><br>
                    <br><br>
                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                <?= form_submit('contacto_submit', (!isset($contacto)?"Crear ":"Modificar ").'Contacto', array('class' => 'btn btn-primary')) ?>
                            </div>
                        </div>
                    </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</body>
