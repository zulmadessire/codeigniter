<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Listado de Contactos</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?= link_tag('assets/css/site.css'); ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="index-contactos m-5">
            <div class="row">
                <div class="col-12">
                    <h2>Listado de Contactos</h2>
                </div>
            </div>

            <br>
            <br>
            <div class="row">
                <div class="col">
                    <div class="text-right">
                        <?= anchor('contactos/create', 'Agregar Contacto', array('title' => 'Agregar Contacto', 'class' => 'btn btn-primary')) ?>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th>Edad</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($listado)) { ?>
                                <?php foreach ($listado as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->id ?></td>
                                        <td><?= $value->nombre ?></td>
                                        <td><?= $value->email ?></td>
                                        <td><?= $value->telefono ?></td>
                                        <td><?= $value->edad ?></td>
                                        <td><?= ($value->estatus == 1)?"Activo":"Inactivo" ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#deleteModal" data-id="<?= $value->id ?>">Eliminar</a> 
                                            <?= anchor('contactos/update/' . $value->id, 'Modificar', array('title' => 'Modificar Contacto')) ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="6"> No hay Resultados </td>
                                </tr>
                            <?php } ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Eliminar Contacto -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar Contacto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('contactos/delete', array('id' => 'delete-form')) ?>
                    <p>Esta seguro que desea eliminar el Contacto.</p>
                    <?= form_input(array('name' => 'id', 'type'=>'hidden', 'id' =>'id')) ?>
                <?= form_close() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <?= form_submit('eliminar_contacto_submit', 'Si', array('class' => 'btn btn-primary', 'form' => 'delete-form')) ?>
            </div>
            </div>
        </div>
    </div>
    <!-- Fin modal-->
</body>

<script>
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        
        if (!!id) {
            $('#id').val(id);
            $('#id').trigger('change');
        }
    });
</script>