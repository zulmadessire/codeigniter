<?php
class Contactos_model extends CI_Model {

    public $id;
    public $email;
    public $nombre;
    public $telefono;
    public $edad;
    public $estatus;

    public function get_one($id)
    {
        $query = $this->db->where('id', $id);
        $query = $this->db->get('contactos');
        return $query->row();
    }

    public function get_all()
    {
        $query = $this->db->get('contactos');
        return $query->result();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('contactos', 10);
        return $query->result();
    }

    public function insert_contactos()
    {
        /*$this->email    = $_POST['email'];
        $this->nombre   = $_POST['nombre'];
        $this->telefono   = $_POST['telefono'];
        $this->edad   = $_POST['edad'];
        $this->estatus   = $_POST['estatus'];*/

        $contacto = $this->input->post();
        unset($contacto['contacto_submit']);

        $this->db->insert('contactos', $contacto);

        return $this->db->insert_id();
    }

    public function update_contacto($id)
    {
        $contacto = $this->input->post();
        unset($contacto['contacto_submit']);

        $this->db->where('id', $id);
        $this->db->update('contactos', $contacto);
    }

    public function delete_contacto($id)
    {
        $this->db->delete('contactos', array('id' => $id));

        return TRUE;
    }

    /*
    public function update_entry()
    {
            $this->title    = $_POST['title'];
            $this->content  = $_POST['content'];
            $this->date     = time();

            $this->db->update('entries', $this, array('id' => $_POST['id']));
    }*/

}