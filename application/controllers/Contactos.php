<?php
class Contactos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    /**
     * Listado de Contactos
     * @return mixed
     */
    public function index()
    {
        $variable['nombre'] = 'Alex';

        $this->load->model('Contactos_model');

        $data['listado'] = $this->Contactos_model->get_all();



        $this->load->view('index_contactos', $data);

    }

    /**
     * Crear un nuevo contacto
     * @return mixed
     */
    public function create()
    {
        $this->load->model('Contactos_model');

        $this->validations();

        if ($this->input->post()) {
            if ($this->form_validation->run() !== FALSE)
            {
                /*echo '<pre>';
                print_r($this->input->post());
                echo '</pre>';*/
                $id_contacto = $this->Contactos_model->insert_contactos();

                if (condition) {
                    redirect('/contactos/index/');
                } else{
                    echo 'Error al insertar el contacto';
                }
            }
        }

        $this->load->view('form_contactos');
    }

    /**
     * Modificar un contacto
     * @return mixed
     */
    public function update($id = NULL)
    {
        if($id !== NULL){
            $this->load->model('Contactos_model');
            $response['contacto'] = $this->Contactos_model->get_one($id);
            
            if (!empty($response['contacto'])) {

                $this->validations();

                if ($this->input->post()) {
                    if ($this->form_validation->run() !== FALSE)
                    {
                        /*echo '<pre>';
                        print_r($this->input->post());
                        echo '</pre>';*/
                        $this->Contactos_model->update_contacto($id);
                        
                        redirect('/contactos/index/');
                    } else{
                        $this->load->view('form_contactos');
                    }
                } else{
                    $this->load->view('form_contactos', $response);
                }
            } else{
                echo "Identidicador no encontrado";
            }
        } else{
            echo "Identificador no especificado";
            return;
        }
    }

    /**
     * Eliminar un contacto
     * @return mixed
     */
    public function delete()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            if ($id == NULL OR !is_numeric($id) ) {
                echo "El id no es valido";
                return;
            } else{
                $this->load->model('Contactos_model');
                $response['contacto'] = $this->Contactos_model->get_one($id);
            
                if (!empty($response['contacto'])) {
                    $this->Contactos_model->delete_contacto($id);
                } else{
                    echo "Contacto no encontrado";
                }
            }
        }

        redirect('/contactos/index/');
    }

    /**
     * Validaciones
     * @return mixed
     */
    public function validations()
    {
        /* Rules */
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[3]|max_length[120]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');
        $this->form_validation->set_rules('edad', 'Edad', 'required|numeric');
        $this->form_validation->set_rules('estatus', 'Estatus', 'required');
        /* End Rules */
    }
}